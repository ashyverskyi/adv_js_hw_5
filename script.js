class Card {
    constructor() {
        this.init();
    }

    async init() {
        try {
            const usersResponse = await fetch("https://ajax.test-danit.com/api/json/users");
            if (!usersResponse.ok) {
                throw new Error("Failed to fetch users");
            }
            const users = await usersResponse.json();
            console.log("User List:", users);
            const postRequests = users.map(user => this.fetchPosts(user));
            const userPosts = await Promise.all(postRequests);
            userPosts.forEach(({ user, posts }) => {
                posts.forEach(post => {
                    this.renderPost(user, post);
                });
            });
        } catch (error) {
            console.error("Error while retrieving user list:", error);
        }
    }

    async fetchPosts(user) {
        try {
            const postsResponse = await fetch(`https://ajax.test-danit.com/api/json/posts?author=${user.id}`);
            if (!postsResponse.ok) {
                throw new Error(`Failed to fetch posts for user ${user.id}`);
            }
            const posts = await postsResponse.json();
            console.log(`Posts of user ${user.name}:`, posts);
            return { user, posts };
        } catch (error) {
            console.error(`Error while retrieving posts of user ${user.name}:`, error);
            return { user, posts: [] };
        }
    }

    renderPost(user, post) {
        const template = document.querySelector('template');
        const postCard = template.content.cloneNode(true);

        const userId = postCard.querySelector('.userId');
        const name = postCard.querySelector('.name');
        const username = postCard.querySelector('.username');
        const email = postCard.querySelector('.e-mail');
        userId.textContent = `User ID: ${user.id}`;
        name.textContent = user.name;
        username.textContent = `aka ${user.username}`;
        email.textContent = user.email;

        const title = postCard.querySelector('.title');
        const postBody = postCard.querySelector('.post-body');
        title.textContent = post.title;
        postBody.textContent = post.body;

        const deleteButton = postCard.querySelector('.delete-post');
        deleteButton.addEventListener('click', async () => {
            try {
                const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${post.id}`, {
                    method: 'DELETE'
                });
                if (!response.ok) {
                    throw new Error('Failed to delete post');
                }
                deleteButton.closest('.main').remove();
                console.log(`Post with ID ${post.id} deleted successfully`);
            } catch (error) {
                console.error('Error deleting post:', error);
            }
        });

        document.body.appendChild(postCard);
    }
}

document.addEventListener("DOMContentLoaded", () => {
    new Card();
});






































